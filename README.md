# Oracle InstantClient Base Image

This repository contains a build process to maintain
base images with [Oracle InstantClient](https://www.oracle.com/database/technologies/instant-client.html)

### Container Images

Dockerfile creates an image with a specific version of Oracle InstantClient.

Maintaining a Dockerfile over time allows maintainers to handle vulnerabilities in a predictable manner:
1. try rebuilding the image as-is, in case the upstream itself has been
patched (this may be accomplished by rerunning a Gitlab CI job)
1. add logic to the Dockerfile (yum update, etc.) to update critical
packages

#### Image Naming Convention

Dockerfile contains two LABELS that are used to name the
image built from it:

- name: oracle-instant-client-base
- version: this must match the exact version of the instantclient software that
  is installed in the image.

Any time the version of the instantclient software is updated, the version LABEL must be changed to match.

The Gitlab CI build uses these to name the image that is built into
the gitlab image registry for deployment.

#### Build Arguments

Dockerfile contains two ARG directives:
- CI_COMMIT_SHA: The sha of the commit of this repo at the
time it is built
- CI_PROJECT_URL: The full url to the gitlab repository (including the
project name itself) where the source and instantclient rpms are
stored. The image will not build if this is not passed!

Images that are published to the gitlab registry, and/or deployed
to kubernetes should contain values for these ARGS. These can be
passed to docker, or kaniko build, by setting them into
environment variables matching their names, and passing the following
flags:

Dockerfile
```
--build-arg CI_COMMIT_SHA=${CI_COMMIT_SHA} --build-arg CI_PROJECT_URL=${CI_PROJECT_URL}
```

The Gitlab CI build supplies these arguments, using the
environment variables set by the Gitlab CI system itself where 
applicable.

## Networking

A sqlnet.ora and ldap.ora, required in 
${ORACLE_HOME}/network/admin, are included in the
image and the repository. These will need to be
maintained over time if the directory servers they
use are changed.

### Gitlab CI

.gitlab-ci.yaml contains jobs to use Gitlab CI to automate all of the
processes required to build, and scan the oracle-instantclient base 
image.

This project uses a master build strategy whereby changes are
always and only pushed to the master branch to effect a build and scan
of the Dockerfile into the base image.

The build depends on an external gitlab projects that maintained by 
DHTS:
- utility/project-templates/ci-templates: maintained by DHTS. The
docker.yml in this gitlab repo contains .kaniko_build
.scan jobs that are extended by the build and scan jobs

#### Build
Each push of changes to the master branch in this gitlab repository
results in an automated build of the image, which is published to
the gitlab image registry for the project. The build is designed to
use the Dockerfile LABELs to name and tag the resulting image, and
pass the CI_COMMIT_SHA and CI_PROJECT_URL build arguments.

#### Scan
Each push of changes to the master branch results in an automated
scan of the image built from the last push to master. The scan uses
the DHTS twistlock scanning service to scan the image, and will fail
if FailVulnerabilities of critical, or high are detected.

## Using the Base Image

All images maintained in this repository are hosted in the
[project gitlab registry](https://gitlab.dhe.duke.edu/ori-rad/ci-pipeline-utilities/oracle-instantclient/container_registry).

### oracle-instantclient
The oracle-instantclient base image can be used as a base image for any
software application that needs oracle instantclient. To do so, create
a Dockerfile in your project working directory with the beginning with
the following line:

```
FROM gitlab.dhe.duke.edu:4567/ori-rad/ci-pipeline-utilities/oracle-instantclient:12.1.0.2.0-1
```

Then extend the base image using [Dockerfile Syntax](https://docs.docker.com/engine/reference/builder/).
The oracle-instantclient image is, itself, built from fedora:31, which 
is a RedHat variant, using the yum package manager. If your application 
uses fedora packages that are not installed in the oracle-instantclient 
base image, you can also add a RUN section to the above to install 
them. It is a good practice to clean up the yum cache after you install 
packages, which reduces the size of the resulting image. See the 
Dockerfile in this repository for a good  example.

