FROM fedora:31

LABEL name 'duke-oracle-instantclient'
LABEL version='12.1.0.2.0-1'

ARG CI_COMMIT_SHA=unspecified
LABEL git_commit=${CI_COMMIT_SHA}

ARG CI_PROJECT_URL
LABEL git_repository_url=${CI_PROJECT_URL}

ENV ORACLE_VERSION '12.1.0.2.0-1'
ENV ORACLE_MAJOR_VERSION '12.1'
ENV PATH="${PATH}:/usr/lib/oracle/${ORACLE_MAJOR_VERSION}/client64/bin"
ENV ORACLE_HOME="/usr/lib/oracle/${ORACLE_MAJOR_VERSION}/client64"
ENV NLS_LANG=AMERICAN_AMERICA.UTF8
ENV LD_LIBRARY_PATH=/usr/lib/oracle/${ORACLE_MAJOR_VERSION}/client64/lib

RUN yum install -y libnsl \
    readline-devel \
    ${CI_PROJECT_URL}/raw/master/instantclient/oracle-instantclient${ORACLE_MAJOR_VERSION}-basic-${ORACLE_VERSION}.x86_64.rpm \
    ${CI_PROJECT_URL}/raw/master/instantclient/oracle-instantclient${ORACLE_MAJOR_VERSION}-sqlplus-${ORACLE_VERSION}.x86_64.rpm \
    ${CI_PROJECT_URL}/raw/master/instantclient/oracle-instantclient${ORACLE_MAJOR_VERSION}-devel-${ORACLE_VERSION}.x86_64.rpm \
    && echo ${LD_LIBRARY_PATH} > /etc/ld.so.conf.d/oracle-instantclient.conf \
    && ldconfig \
    && rm -rf /var/tmp/yum* \
    && yum clean all

